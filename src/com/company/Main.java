package com.company;

import com.company.Exceptions.IncorrectInputException;

public class Main {

    public static void main(String[] args) {
        IOHandler.displayMenu();
        try {
            IOHandler.handleInput();
        } catch (IncorrectInputException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
