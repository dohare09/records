package com.company;

import com.company.Exceptions.IncorrectInputException;

import java.util.Scanner;

/**
 * This is the class that will handle all IO of the program
 */
public class IOHandler {
    static Scanner sc = new Scanner(System.in);
    public static void handleInput() throws IncorrectInputException {
        String input = sc.next().toLowerCase();
        switch (input) {
            case "1" : System.out.println("1"); break;
            case "2" : System.out.println("2"); break;
            case "comp" : System.out.println("Comp"); break;
            case "competition" : System.out.println("Competition"); break;
            case "lifter" : System.out.println("Lifter"); break;
            default: throw new IncorrectInputException();
        }
    }

    public static void displayMenu() {
        System.out.println("---NIWL Records---\nPlease select an option\n1. Competition\n2. Lifter");
    }
}

