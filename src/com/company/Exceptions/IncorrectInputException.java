package com.company.Exceptions;

public class IncorrectInputException extends Exception {
    public IncorrectInputException() {
        super("The input given is not valid");
    }
}
